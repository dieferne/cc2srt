#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct tiempo_cc{
    int minutos;
    int segundos;
};
struct entrada_cc{
    struct tiempo_cc tiempo;
    char texto[255];
};

struct tiempo_srt{
    char hora[3];
    char minuto[3];
    char segundo[3];
};

struct salida_srt{
    int index;
    struct tiempo_srt inicio;
    struct tiempo_srt fin;
    char texto[255];
};

int LeeEntrada(struct entrada_cc *ent, FILE *fentrada){
    if (fscanf((FILE *)fentrada, "%d:%d\n", &ent->tiempo.minutos, &ent->tiempo.segundos) < 1) {
        return 0;
    }
    fgets(ent->texto, 255, (FILE *)fentrada);
    return 1;
}

void ConvierteTiempo(struct tiempo_cc *entrada, struct tiempo_srt *salida) {
    sprintf(salida->hora, "%02d", entrada->minutos/60);
    sprintf(salida->minuto, "%02d", entrada->minutos%60);
    sprintf(salida->segundo, "%02d", entrada->segundos);
}

void ImprimeSalida(char *cadena, struct salida_srt *sal){
        sprintf(cadena, "%d\n%s:%s:%s --> %s:%s:%s\n%s\n", sal->index, sal->inicio.hora, sal->inicio.minuto, sal->inicio.segundo, sal->fin.hora, sal->fin.minuto, sal->fin.segundo, sal->texto);
    
}

int main(int argc, char **argv) {
    FILE *entrada;
    FILE *salida;
    char cadena [255] = "salida.srt";
    struct entrada_cc ent;
    struct tiempo_cc tAnterior;
    struct salida_srt sal;

    if (argc < 2) {
        printf("Uso: %s <nombre de fichero a convertir>.\n", argv[0]);
        return(3);
    }
    entrada = fopen(argv[1], "rt");
    if (entrada == NULL) {
        printf("El fichero %s no se ha podido abrir.\n", argv[1]);
        return(1);
    }
    salida = fopen(cadena, "w");
    if (salida == NULL) {
        printf("El fichero %s no se ha podido abrir.\n", argv[1]);
        fclose(entrada);
        return(2);
    }
    printf("Procesando fichero %s.\n", argv[1]);
    fgets(cadena, 256, (FILE *) entrada);
    LeeEntrada(&ent, (FILE *)entrada);
    sal.index = 1;
    ConvierteTiempo(&ent.tiempo, &sal.inicio);
    strcpy(sal.texto, ent.texto);
    while(LeeEntrada(&ent, (FILE *)entrada) > 0) {
        ConvierteTiempo(&ent.tiempo, &sal.fin);
        ImprimeSalida(cadena, &sal);
        fputs(cadena, (FILE *) salida);
        sal.index++;
        ConvierteTiempo(&ent.tiempo, &sal.inicio);
        strcpy(sal.texto, ent.texto);
    }

    fclose(entrada);
    fclose(salida);
    return(0);
}
