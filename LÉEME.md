#Conversor de Closed Captions a SubRib
Los subtítulos autogenerados de Google se pueden descargar pero su formato
no es estándar y no se reconoce por los reproductores que utilizo (al menos VLC
no lo hace) así que esta es una utilidad para convertirlos al formato srt que es "más"
universal.
##Uso
Proporcione el nombre del fichero de los subtítulos de Google y el programa generará un fichero de nombre salida.srt en la carpeta de trabajo.
##¿Cómo obtener el fichero con los subtítulos de Google?
Hasta donde yo sé el proceso es bastante manual, pero funciona. Si alguien conoce alguna forma de realizarlo más automática estaría encantado de tratar de implementarlo.
1. Abrir en el navegador la página de Youtube con el vídeo en que estamos interesados.
2. Activar los subtítulos generados automáticamente.
3. Debajo del vídeo aparece un menú con las opciones Me gusta, Guardar y un menú con tres puntos en horizontal.
4. Entramos en el menú de los tres puntos y activamos la opción Ver transcripción.
5. En la ventana que nos aparece seleccionamos todo el texto y lo pegamos en un archivo que será el que introducimos en la utilidad cc2srt.
###Nota sobre el formato del fichero de entrada.
En las pruebas que hice, el texto copiado incluye una línea en blanco en primer lugar. El programa espera que exista esa línea al comienzo del fichero. Si no existe fallará.
